#!/bin/bash -e
#
# File: /var/lib/jenkins/jenkins-java-remote-build/jenkins-java-remote-build.sh -> initiates and builds projects in Jenkins server unto a remote server
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash jenkins-java-remote-build.sh <BUILD_COMMAND> <LOCAL_PROJECT_NAME> <REMOTE_PROJECT_DATABASE> <REMOTE_PROJECT_BUILD_DIR> <REMOTE_APP_SERVICE_UNIT> <IP_OF_REMOTE_SERVER>
#
#

## - start here

decomposeApplicationNameTo ()
{
  grep -iP -A4 '\<groupid\>com\.minex360' $LOCAL_PROJECT_BUILD_CONFIG|\
  grep "\<$1"|sed "s/${1}//g;s/<//g;s/>//g;s/\///g"|tr -d '[:space:]'
}

buildApplicationNames ()
{
  local appArtifact=$(decomposeApplicationNameTo "artifactId")
  local appVersion=$(decomposeApplicationNameTo "version")
  local appPackaging=$(decomposeApplicationNameTo "packaging")
  APP="${appArtifact}-${appVersion}.${appPackaging}"
  REMOTE_BACKUP_APP="${appArtifact}-${appVersion}.${BUILD_RUNTIME}.${appPackaging}"
}

runRemoteCommand ()
{
  $REMOTE_SERVER_COMMAND_RUNNER "$1"
}

remoteFileExists ()
{
  $REMOTE_SERVER_COMMAND_RUNNER <<EOF
if [[ -e "${1}" ]]
then
  echo 0
else
  echo 1
fi
EOF
}

backupRemoteProject ()
{
  runRemoteCommand "zip -r "${remoteProjectBackupDestination}" "${remoteProjectBackupSource}" 2>&1"
}

removeRemoteProject ()
{
  runRemoteCommand "rm -rf "${REMOTE_PROJECT}" 2>&1"
}

backupRemoteProjectDatabase ()
{
  $REMOTE_SERVER_COMMAND_RUNNER <<EOF
  pg_dump -w -U postgres --format=c --data-only --file="${REMOTE_PROJECT_DATABASE_BACKUP_DATA}" \
    "${REMOTE_PROJECT_DATABASE}" # dump project database data
  pg_dump -w -U postgres --format=c --schema-only --create --file="${REMOTE_PROJECT_DATABASE_BACKUP_SCHEMA}" \
    "${REMOTE_PROJECT_DATABASE}" # dump project database schema
EOF
}

createRemoteDirectories ()
{
  $REMOTE_SERVER_COMMAND_RUNNER <<EOF
  for directory in \
    "${REMOTE_JENKINS_WORKSPACE_DIR}" \
    "${REMOTE_JENKINS_BACKUP_DIR}" \
    "${REMOTE_JENKINS_BACKUP_SOURCE_DIR}" \
    "${REMOTE_JENKINS_BACKUP_DB_DIR}" \
    "${REMOTE_JENKINS_BACKUP_APPS_DIR}" \
    "${REMOTE_PROJECT}" \
    "${REMOTE_PROJECT_DATABASE_BACKUP}" \
    "${REMOTE_PROJECT_APP_BACKUP}"
  do
    if [ ! -d \$directory ]
    then
      mkdir -vp \$directory
    fi
  done

  if [[ ! -d  "${REMOTE_PROJECT_BUILD_SOURCE_DIR}" ]]
  then
    sudo mkdir -vp "${REMOTE_PROJECT_BUILD_SOURCE_DIR}"
  fi
EOF
}

backupAndBuildAndDeployRemoteProject ()
{
  $REMOTE_SERVER_COMMAND_RUNNER <<EOF
  # backup running build
  sudo cp -ap "${REMOTE_PROJECT_BUILD_TARGET_DIR}/${APP}" "${REMOTE_PROJECT_APP_BACKUP}/${REMOTE_BACKUP_APP}"
  
  # test app backup is successful
  if [[ ! -e "${REMOTE_PROJECT_APP_BACKUP}/${REMOTE_BACKUP_APP}" ]]
  then
    echo >&2 "No '${REMOTE_PROJECT_BUILD_TARGET_DIR}/${APP}' to backup."
  fi
  
  # copy project to build dir
  sudo cp -ap "${REMOTE_PROJECT}"/* "${REMOTE_PROJECT_BUILD_SOURCE_DIR}"

  # copy project application properties
  sudo cp -ap "${APP_PROPERTIES_FILE}" "${REMOTE_PROJECT_BUILD_SOURCE_DIR}"

  # change to remote build source directory
  cd "${REMOTE_PROJECT_BUILD_SOURCE_DIR}"

  # build project
  sudo mvn clean install package

  # revert to using latest working version before build process
  [[ ! -e "${REMOTE_PROJECT_BUILD_TARGET_DIR}/${APP}" ]] && {
    echo >&2 "$COMMAND: Build and deploy failed."
    echo >&2 "$COMMAND: Reverting to latest working version..."
    sudo cp -ap "${REMOTE_PROJECT_APP_BACKUP}/${REMOTE_BACKUP_APP}" "${REMOTE_PROJECT_BUILD_TARGET_DIR}/${APP}" 
  }

  # change ownership to project owner
  sudo chown -R "${PROJECT_OWNER}":"${PROJECT_OWNER}" "${REMOTE_PROJECT_BUILD_SOURCE_DIR}"

  # pause build
  sleep "${BUILD_DEPLOY_PAUSE}"

  # stop application
  sudo systemctl stop "${APP_SERVICE_UNIT}"

  # start application
  sudo systemctl start "${APP_SERVICE_UNIT}"

  # show status of application
  sudo systemctl status "${APP_SERVICE_UNIT}"

  count=0
  
  until [[ \$count == ${SHOW_RUNTIME_LOGS} ]]
  do
    sudo journalctl -n ${SHOW_RUNTIME_LOGS_NUMBER} -u "${APP_SERVICE_UNIT}"|tee
    sleep "${BUILD_DEPLOY_PAUSE}"
    ((count++))
  done

  # show application is running
  sudo netstat -tulpn|grep -P ":${APP_SERVICE_PORT}"

  echo >&2 "$COMMAND: Build and deploy done."

  exit 0
EOF
}

COMMAND=$0
BUILD_COMMAND=$1
LOCAL_PROJECT_NAME=$2
REMOTE_PROJECT_DATABASE=$3
REMOTE_PROJECT_BUILD_DIR=${4:-'/opt/minex/api'}
REMOTE_APP_SERVICE_UNIT=$5
REMOTE_SERVER_IP=$6
RUNTIME_AT=`date +'%Y%m%d%H%M%S'`
BUILD_RUNTIME=${RUNTIME_AT}
DEPLOY_RUNTIME=${RUNTIME_AT}

# check number of arguments passed
if [[ "$#" != 6 ]]
then
  echo >&2 "$COMMAND: expects five arguments i.e. <BUILD_COMMAND> <LOCAL_PROJECT_NAME> <REMOTE_PROJECT_DATABASE> <REMOTE_PROJECT_BUILD_DIR> <REMOTE_APP_SERVICE_UNIT> <IP_OF_REMOTE_SERVER>"
  exit 1
fi

: ${REMOTE_SERVER_SSH_PORT=3600}
: ${JENKINS_WORKSPACE='/var/lib/jenkins/workspace'}
: ${JENKINS_USER='jenkins'}
: ${RSYNC_SSH="ssh -p ${REMOTE_SERVER_SSH_PORT}"}
: ${REMOTE_JENKINS_HOME='/home/jenkins'}
: ${REMOTE_JENKINS_WORKSPACE_DIR="${REMOTE_JENKINS_HOME}/workspace"}
: ${REMOTE_JENKINS_BACKUP_DIR="${REMOTE_JENKINS_HOME}/backup"}
: ${REMOTE_JENKINS_BACKUP_SOURCE_DIR="${REMOTE_JENKINS_BACKUP_DIR}/src"}
: ${REMOTE_JENKINS_BACKUP_DB_DIR="${REMOTE_JENKINS_BACKUP_DIR}/postgres"}
: ${REMOTE_JENKINS_BACKUP_APPS_DIR="${REMOTE_JENKINS_BACKUP_DIR}/apps"}
: ${REMOTE_PROJECT_BUILD_SOURCE_DIR="${REMOTE_PROJECT_BUILD_DIR}/${LOCAL_PROJECT_NAME}"}
: ${REMOTE_PROJECT_BUILD_TARGET_DIR="${REMOTE_PROJECT_BUILD_DIR}/${LOCAL_PROJECT_NAME}/target"}
: ${REMOTE_SERVER_COMMAND_RUNNER="ssh -T -p ${REMOTE_SERVER_SSH_PORT} ${JENKINS_USER}@${REMOTE_SERVER_IP}"}
: ${APP_PROPERTIES_FILE="${REMOTE_PROJECT_BUILD_DIR}/application.properties"}
: ${APP_SERVICE_UNIT=$REMOTE_APP_SERVICE_UNIT}
: ${APP_SERVICE_PORT=8443}
: ${PROJECT_OWNER='minex360'}
: ${BUILD_DEPLOY_PAUSE=2}
: ${SHOW_RUNTIME_LOGS=3}
: ${SHOW_RUNTIME_LOGS_NUMBER=100}

LOCAL_PROJECT="${JENKINS_WORKSPACE}/${LOCAL_PROJECT_NAME}"
REMOTE_PROJECT="${REMOTE_JENKINS_WORKSPACE_DIR}/${LOCAL_PROJECT_NAME}"
REMOTE_PROJECT_DATABASE_BACKUP="${REMOTE_JENKINS_BACKUP_DB_DIR}/${REMOTE_PROJECT_DATABASE}"
REMOTE_PROJECT_APP_BACKUP="${REMOTE_JENKINS_BACKUP_APPS_DIR}/${LOCAL_PROJECT_NAME}"
REMOTE_PROJECT_DATABASE_BACKUP_SCHEMA="${REMOTE_PROJECT_DATABASE_BACKUP}/jenkins-java-remote-build-${REMOTE_PROJECT_DATABASE}.${BUILD_RUNTIME}.schema"
REMOTE_PROJECT_DATABASE_BACKUP_DATA="${REMOTE_PROJECT_DATABASE_BACKUP}/jenkins-java-remote-build-${REMOTE_PROJECT_DATABASE}.${BUILD_RUNTIME}.data"
LOCAL_PROJECT_BUILD_CONFIG="${LOCAL_PROJECT}/pom.xml"
APP=
REMOTE_BACKUP_APP=

# check build command
if ! echo -n "$BUILD_COMMAND" | grep -qP '^(build|deploy|rollback)$'
then
  echo >&2 "$COMMAND: Build command '$BUILD_COMMAND' invalid. Should be 'build/deploy/rollback'"
  exit 1
fi

# check project's existence
if [[ ! -d "$LOCAL_PROJECT" ]]
then
  echo >&2 "$COMMAND: $LOCAL_PROJECT not found."
  exit 1
fi

# check remote server ip
if ! echo -n "$REMOTE_SERVER_IP" | grep -qP '^\d{3}\.\d{3}\.\d{3}\.\d{3}$'
then
  echo >&2 "$COMMAND: Remote IP '$REMOTE_SERVER_IP' invalid."
  exit 1
fi

# create remote directories if there are none
createRemoteDirectories

# check whether project exists on remote server
projectExists=$(remoteFileExists "$REMOTE_PROJECT")

if [[ $projectExists == 0 ]]
then
  # backup old project
  echo "Backup remote project '${REMOTE_PROJECT}'..."
  remoteProjectBackupDestination="${REMOTE_JENKINS_BACKUP_DIR}/${LOCAL_PROJECT_NAME}.${BUILD_RUNTIME}.zip"
  remoteProjectBackupSource="${REMOTE_PROJECT}"
  backupRemoteProject
  backupSucceeded=$(remoteFileExists "$remoteProjectBackupDestination")
  
  if [[ $backupSucceeded != 0 ]]
  then
    echo >&2 "$COMMAND: Remote backup of $LOCAL_PROJECT_NAME failed."
    exit 1
  fi

  # remove old project from REMOTE_JENKINS_WORKSPACE_DIR
  echo "Remove remote project '${REMOTE_PROJECT}'..."
  removeRemoteProject
  removeSucceeded=$(remoteFileExists "$REMOTE_PROJECT")
  
  if [[ $removeSucceeded != 1 ]]
  then
    echo >&2 "$COMMAND: Could not remove $LOCAL_PROJECT_NAME after backup."
    exit 1
  fi
fi

# copy project with latest changesets to remote dir
echo "Copy ${LOCAL_PROJECT} to ${REMOTE_JENKINS_WORKSPACE_DIR}..."
rsync -av --progress -e "$RSYNC_SSH" --exclude='.git/' --exclude='target/' \
  ${LOCAL_PROJECT} ${JENKINS_USER}@${REMOTE_SERVER_IP}:${REMOTE_JENKINS_WORKSPACE_DIR}

# backup database
echo "Backup project database '$REMOTE_PROJECT_DATABASE' to ${REMOTE_PROJECT_DATABASE_BACKUP}..."
backupRemoteProjectDatabase
backupSchemaSucceeded=$(remoteFileExists "$REMOTE_PROJECT_DATABASE_BACKUP_SCHEMA")
backupDataSucceeded=$(remoteFileExists "$REMOTE_PROJECT_DATABASE_BACKUP_DATA")

if [[ "$backupSchemaSucceeded" != 0 && "$backupDataSucceeded" != 0 ]]
then
  echo >&2 "$COMMAND: Project database '$REMOTE_PROJECT_DATABASE' backup failed."
  exit 1
fi

echo >&2 "$COMMAND: Project database '$REMOTE_PROJECT_DATABASE' backup successful."

# backup, build and deploy project
echo "Backup, build and deploy ${LOCAL_PROJECT_NAME}..."
buildApplicationNames
backupAndBuildAndDeployRemoteProject

exit 0

## -- finish
